---
title: "VE Tactics Sympathy"
author: "Dan Killian"
date: "2021-01-07"
output: 
  workflowr::wflow_html:
    code_folding: hide
editor_options:
  chunk_output_type: console
---

#####

```{r global_options, include=T, warning=F, message=F, echo=T, error=F}

# standard figure size and generate clean output
knitr::opts_chunk$set(fig.width=8, fig.height=6, warning=FALSE, message=FALSE, cache=TRUE, error=T, echo=T)

source("code/BiH analysis prep.R")

```


```{r}

tac1 <- stan_glm(cv_bin3 ~ unemployed + never_married + female + age + age_sq,
               data=dat,
               refresh=0)

summary(tac1, digits=3)
plot(tac1)

```


```{r}

frq(dat$Q30_3_bin)

tac2 <- stan_glmer(cv_bin3 ~ unemployed + never_married + female + age + age_sq + (1|ethnic),
               data=dat,
               refresh=0)

summary(tac2, digits=3)
plot(tac2)
ranef(tac2)
plot(ranef(tac2))

tab_model(tac2)

plot_model(tac2)

```

```{r}
#frq(dat$Q33)

tac_l1 <- lm(cv_bin ~ educ + unemployed + never_married + female + age + age_sq + ses + bribe + Q2_social + help + message_rate + unsafe,
         data=dat)

summary(tac_l1)

```

```{r}

plotreg(tac_l1,
        omit.coef = "(Intercept)")

```









