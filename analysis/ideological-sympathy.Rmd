---
title: "VE Ideological Sympathy"
author: "Dan Killian"
date: "2021-01-07"
output: 
  workflowr::wflow_html:
    code_folding: hide
editor_options:
  chunk_output_type: console
---

#####

```{r global_options, include=T, warning=F, message=F, echo=T, error=F}

# standard figure size and generate clean output
knitr::opts_chunk$set(fig.width=8, fig.height=6, warning=FALSE, message=FALSE, cache=TRUE, error=T, echo=T)

source("code/BiH analysis prep.R")

```

#### base model

```{r}

r1 <- stan_glm(jihad ~ unemployed + never_married + female + age + age_sq,
               data=dat,
               refresh=0)

summary(r1, digits=3)

```

```{r}
r2 <- stan_glm(jihad ~ educ + unemployed + never_married + female + age + age_sq,
               data=dat,
               refresh=0)

summary(r2, digits=3)
```

```{r}

frq(dat$Q13)

r3 <- stan_glm(jihad ~ educ + unemployed + never_married + female + age + age_sq + unsafe,
               data=dat,
               refresh=0)

summary(r2, digits=3)
```


```{r}
frq(dat$Q33)

l1 <- lm(jihad ~ educ + unemployed + never_married + female + age + age_sq + ses + bribe + social + help + message_rate + unsafe,
         data=dat)

summary(l1)

```

```{r}

plotreg(l1,
        omit.coef = "(Intercept)")

```


```{r}
frq(dat$Q33)

s1 <- svyglm(jihad ~ educ + unemployed + never_married + female + age + age_sq + ses + bribe + Q2_social + member_grps + help + message_rate + unsafe,
         design=svydat)

summary(s1)

```



#### Safety and Security
##### Feels unsafe in community

```{r}

safe1 <- stan_glm(unsafe ~ unemployed + never_married + female + age + age_sq,
               data=dat,
               refresh=0)

summary(safe1, digits=3)

```


```{r}

safe2 <- stan_glmer(unsafe ~ unemployed + never_married + female + age + age_sq + (1|ethnic),
               data=dat,
               refresh=0)

summary(safe2, digits=3)

```

```{r}

safe3 <- stan_glmer(unsafe ~ unemployed + never_married + female + age + age_sq + (1|entity),
               data=dat,
               refresh=0)

summary(safe3, digits=3)

```

##### Police treat people equally under law

```{r}

eq1 <- stan_glm(equal ~ unemployed + never_married + female + age + age_sq,
               data=dat,
               refresh=0)

summary(eq1, digits=3)
plot(eq1)

```



