---
title: "Sociality"
author: "Dan Killian"
date: "2021-02-01"
output: 
  workflowr::wflow_html:
    code_folding: hide
editor_options:
  chunk_output_type: console
---

#####

```{r global_options, include=T, warning=F, message=F, echo=T, error=F}

# standard figure size and generate clean output
knitr::opts_chunk$set(fig.width=8, fig.height=6, warning=FALSE, message=FALSE, cache=TRUE, error=T, echo=T)

source("code/BiH analysis prep.R")

```

Sociality is measured by a higher contact rate across ethnic or religious groups. A majority of respondents report social contact across ethnic or religious groups at least once a month. 

```{r}

q2 <- svyrdat %>%
  group_by(Q2) %>%
  summarise(Percent = survey_mean(na.rm=T),
            sample = survey_total(na.rm=T)) %>%
  na.omit %>%
  mutate(Category=get_labels(Q2)[1:5],
         Margin = 1.96*Percent_se) %>%
  select(Category,Percent, Margin, `Weighted sample` = sample) %>%
  gt() %>%
  fmt_percent(2:3, decimals=1)

q2

```


```{r}

#frq(dat$ethnic)

q2_eth <- svyby(~Q2_social,
                ~ethnic,
                FUN=svymean,
                na.rm=T,
                design=svydat) %>%
  mutate(Ethnicity = get_labels(dat$ethnic),
         Margin = 1.96*se) %>%
  select(Ethnicity, Percent=Q2_social, Margin) %>%
  gt() %>%
  fmt_percent(2:3, decimals=1) %>%
  tab_header(html("Q2. Socialize with people from other<br>ethnic or religious groups, by ethnicity")) %>%
  tab_source_note("Overall mean 42%")

q2_eth

```


```{r}

#frq(dat$ethnic)

q2_ent <- svyby(~Q2_social,
                ~entity,
                FUN=svymean,
                na.rm=T,
                design=svydat) %>%
  mutate(Entity = get_labels(dat$entity),
         Margin = 1.96*se) %>%
  select(Entity, Percent=Q2_social, Margin) %>%
  gt() %>%
  fmt_percent(2:3, decimals=1) %>%
  tab_header(html("Q2. Socialize with people from other<br>ethnic or religious groups, by entity")) %>%
  tab_source_note("Overall mean 42%")

q2_ent

```


```{r}

#frq(dat$ethnic)

soc_s1 <- svyglm(Q2_social ~ educ + urban + never_married + unemployed + female + age + age_sq + ses + member_grps + unsafe,
                 design=svydat)

summary(soc_s1)

```

```{r}

plotreg(soc_s1,
        omit.coef = "(Intercept)")

```

```{r}

soc_s1_T <- tidy(soc_s1) %>%
  mutate(lower = estimate - 1.96*std.error,
         upper = estimate + 1.96*std.error,
         color=case_when(lower > 0 ~ "darkblue",
                         upper < 0 ~ "maroon",
                         TRUE ~ "grey60"),
         term_lab=c("Intercept",
                     "Level of education",
                     "Urban residence",
                     "Never married",
                     "Unemployed",
                     "Female",
                     "Age",
                     "Age squared",
                     "Socio-economic status",
                     "Number of social groups",
                     "Feels unsafe in community")) %>%
  filter(term_lab != "Intercept", 
         term_lab != "Age squared") %>%
  arrange(color)

#soc_s1_T

# ggplot(soc_s1_T, aes(x=estimate, y=fct_reorder(term_lab, estimate))) + 
#   geom_vline(xintercept=0, color="darkgrey", size=1.2) + 
#   geom_errorbarh(aes(xmin=lower, xmax=upper, color=color), height=0, size=.9) +
#   geom_label(aes(color=color, label=paste(100*round(estimate,2), "%", sep=""), size=3)) +
#   scale_color_manual(values=c("darkblue", "grey60", "maroon")) +
#   guides(color=F,
#          size=F) 

ggplot(soc_s1_T, aes(x=estimate, y=fct_reorder(term_lab, estimate))) + 
  geom_vline(xintercept=0, color="darkgrey", size=1.2) + 
  geom_errorbarh(aes(xmin=lower, xmax=upper, color=color), height=0, size=.9) +
  geom_label(aes(color=color, label=round(estimate,2)), size=4) +
  scale_color_manual(values=c("darkblue", "grey60", "maroon")) +
  scale_x_continuous(limits=c(-.2, .2)) + 
  guides(color=F,
         size=F) +
  labs(x="",
       y="")

```

```{r}

cplot(soc_s1, "educ", what="prediction")

```

```{r}

a <- cplot(soc_s1, "educ") %>%
  mutate(type="predicted")

a

ed <- svyrdat %>%
  group_by(xvals=educ) %>%
  summarise(yvals=survey_mean(Q2_social, na.rm=T)) %>%
  na.omit() %>%
  mutate(upper = yvals+1.96*yvals_se,
         lower = yvals - 1.96*yvals_se,
         type="observed") %>%
  select(-3)

ed_marg <- a %>%
  rbind(ed) %>%
  arrange(xvals)
  
ed_marg

ggplot(ed_marg, aes(x=xvals, y=yvals, color=type)) +
  stat_smooth(se=F) + 
  scale_color_manual(values=c("dodgerblue", "firebrick")) + 
  scale_x_continuous(breaks=0:7) + 
  scale_y_continuous(limits=c(.1,.6),
                     breaks=seq(.1,.6,.1),
                     labels=percent_format(accuracy=1)) + 
  labs(x="Level of education",
       y="Social",
       title="Estimated relationship between education and sociality",
       caption="Q2. Socialize with members of other religious or ethnic groups at least once a month\nPredicted models control for locality, marital status, employment status, sex, age,\nsocio-economic status, sense of personal safety, and social group membership")

```


```{r}

# soc_l1 <- lm(Q2_social ~ educ + urban + never_married + unemployed + female + age + age_sq + ses + member_grps + unsafe,
#                  dat=dat)
# 
# summary(soc_l1)

```

```{r}

# cplot(soc_l1, "educ")

```

#### Sociality and VE tactics sympathy

```{r}

#frq(dat$cv_fac

soc_tac1 <- svyglm(cv_fac_bin ~ Q2_social,
                 design=svydat)

summary(soc_tac1)

```

```{r}

#frq(dat$cv_fac

soc_tac2 <- svyglm(cv_fac_bin ~ Q2_social + educ + urban + never_married + unemployed + female + age + age_sq + ses + member_grps + unsafe,
                 design=svydat)

summary(soc_tac2)

```

```{r}

plotreg(soc_tac2,
        omit.coef = "(Intercept)")

```

#### Sociality and ideological sympathy

```{r}

#frq(dat$jihad)

soc_ideo1 <- svyglm(jihad ~ Q2_social,
                 design=svydat)

summary(soc_ideo1)

```

```{r}

#frq(dat$cv_fac

soc_ideo2 <- svyglm(jihad ~ Q2_social + educ + urban + never_married + unemployed + female + age + age_sq + ses + member_grps + unsafe,
                 design=svydat)

summary(soc_ideo2)

```

```{r}

plotreg(soc_ideo2,
        omit.coef = "(Intercept)")

```


#### Sociality and VEO sympathy

```{r}

#frq(dat$Q31_1_positive)
#frq(dat$Q31_2_positive)

soc_veo1 <- svyglm(veo_symp ~ Q2_social,
                 design=svydat)

summary(soc_veo1)

```

```{r}

#frq(dat$cv_fac

soc_veo2 <- svyglm(veo_symp ~ Q2_social + Q2_social:as.factor(ethnic) + educ + urban + never_married + unemployed + female + age + age_sq + ses + member_grps + unsafe,
                 design=svydat)

summary(soc_veo2)

```

```{r}

plotreg(soc_veo2,
        omit.coef = "(Intercept)")

```

#### Sociality and ethno-nationalist sympathy

```{r}

#frq(dat$Q32_1_positive)

soc_ethno1 <- svyglm(Q32_1_positive ~ Q2_social:as.factor(ethnic),
                 design=svydat)

summary(soc_ethno1)

```

```{r}

#frq(dat$ethnic)

soc_ethno2 <- svyglm(Q32_1_positive ~ Q2_social:as.factor(ethnic) + educ + urban + never_married + unemployed + female + age + age_sq + ses + member_grps + unsafe,
                 design=svydat)

summary(soc_ethno2)

```

```{r}

plotreg(soc_ethno2,
        omit.coef = "(Intercept)")

```


```{r}
#frq(datL$ethnic)
#frq(datL$Q2)

test <- lm(Q2_social ~ endline,
       data = datL)

summary(test)

```


```{r}
#frq(datL$ethnic)
#frq(datL$Q2)

test2 <- lm(Q2_social ~ endline:as.factor(ethnic),
       data = datL)

summary(test2)

```


