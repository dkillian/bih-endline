**********************************************************************************************************		
	* 	Gustavo Castillo
	*	This do file: 
		*Labels and organizes variables
		*Performs logic checks
	*	Dec 2020									    		
	*	Uses: .dta
	* 	Saves: BiHCVEMidSurv2020_boost_raw.dta
**************************************************************************************************************

*set working DIRECTORIES 	
global dirfolder "C:\Users\gustavo.castillo\Desktop\CVE Midline Survey Questionnaire for Bosnia and Herzegovina" 

*IMPORT data
clear
use "$dirfolder\Data\Midline\raw data\BiHCVEMidSurv2020_boost_raw.dta", clear

*CHECK import of all necessary variables
ssc install unique
unique SbjNum // 3390 unique codes

*CONVERT strings to numeric 
gen EnumGender1=.
replace EnumGender1=1 if EnumGender=="M"
replace EnumGender1=2 if EnumGender=="F"

label def gend 1 "Male" 2 "Female"
label val EnumGender1 gend

gen EnumEtn1=.
replace EnumEtn1=1 if EnumEtn=="BOŠNJAK"
replace EnumEtn1=2 if EnumEtn=="HRVAT"
replace EnumEtn1=3 if EnumEtn=="SRBIN" 

label def ethint 1 "Bosnian" 2 "Croatian" 3 "Serbian"
label val EnumEtn1 ethint

gen SupervGender1=.
replace SupervGender1=1 if SupervGender=="M"
replace SupervGender1=1 if SupervGender=="F"
label val SupervGender1 gend

gen SupervEtn1=.
replace SupervEtn1=1 if SupervEtn=="BOŠNJAK"
replace SupervEtn1=2 if SupervEtn=="HRVAT"
replace SupervEtn1=3 if SupervEtn=="SRBIN"
label val SupervEtn1 ethint

*Checking for conditional - skip patterns and logic checks
tab Q4,m
tab Q4a if Q4==1

tab Q12,m
tab Q12_opn if Q12==10

tab Q12,m
tab Q12a if Q12==8

tab Q24,m
tab Q24a_1 if Q24==2 | Q24==3 | Q24==4

tab Q24a_1,m
tab Q24a_opn if Q24a_1==5

tab Q25,m
tab Q25a_1 if Q25==2 | Q25==3 | Q25==4

tab Q25a_1,m
tab Q25a_opn if Q25a_1==5

tab Q31,m
tab Q31a if Q31==1

tab Q31,m
tab Q31b if Q31==1

tab Q32,m
tab Q32a if Q32==1

tab Q33,m
tab Q33_1 if Q33!=1

tab Q33_1,m
tab Q33b_1 if Q33_1==1

tab Q33b_1,m
tab Q33b_1_opn if Q33b_1==1

tab Q33_1
tab Q33c_1 if Q33_1==1

tab Q33c_1,m
tab Q33b_1_opn if Q33c_1==1

tab Q33c_1,m
tab Q33c_1_opn if Q33c_1==1  

tab Q33_1,m
tab Q33b_1_cod if Q33_1==1

tab Q33_1,m
tab Q33c_1_cod if Q33_1==1

tab Q33_2,m
tab Q33b_2 if Q33_2==1

tab Q33_2,m
tab Q33b_2_opn if Q33_2==1

tab Q33_2,m
tab Q33c_2 if Q33_2==1

tab Q33_2,m
tab Q33c_2_opn if Q33_2==1

tab Q33_2,m
tab Q33b_2_cod if Q33_2==1

tab Q33_2,m
tab Q33c_2_cod if Q33_2==1

tab Q33_3,m
tab Q33_3_opn if Q33_3==1

tab Q33_3,m
tab Q33b_3 if Q33_3==1

tab Q33_3,m
tab Q33b_3_opn if Q33_3==1

tab Q33_3,m
tab Q33c_3 if Q33_3==1

tab Q33_3,m
tab Q33c_3_opn if Q33_3==1

tab Q33_3,m
tab Q33a_3_cod if Q33_3==1

tab Q33_3,m
tab Q33b_3_cod if Q33_3==1

tab Q33_3,m
tab Q33c_3_cod if Q33_3==1

tab Q33_4,m
tab Q33_4_opn if Q33_3==1

tab Q33_4,m
tab Q33b_4 if Q33_3==1

tab Q33_4,m
tab Q33b_4_opn if Q33_3==1

tab Q33_4,m
tab Q33c_4 if Q33_3==1

tab Q33_4,m
tab Q33c_4_opn if Q33_3==1

tab Q33_4,m
tab Q33a_4_cod if Q33_3==1

tab Q33_4,m
tab Q33b_4_cod if Q33_3==1

tab Q33_4,m
tab Q33c_4_cod if Q33_3==1

tab Q36,m
tab Q36_opn if Q36==7

tab Q38,m
tab Q38_opn if Q38==7

tab Q39_9x,m
tab Q39_9_opn if Q39_9x==1

tab Q48,m
tab Q48_opn if Q48==3

*tab Q4,m
*tab Q15 if Q4==1 (important - this one does not match)

*tab Q47,m // The n of observations for Q47a do not match those of Q47
*tab Q47a,m

tab Q38,m
tab Q41 if Q38!=5 | Q38!=6

tab Q38,m
tab Q42 if Q38!=5 | Q38!=6

tab Q38,m
tab Q43_1 if Q38!=5 | Q38!=6

tab Q38,m
tab Q43_2 if Q38!=5 | Q38!=6

tab Q38,m
tab Q43_3 if Q38!=5 | Q38!=6

tab Q38,m
tab Q44 if Q38!=5 | Q38!=6

tab Q38,m
tab Q45 if Q38!=5 | Q38!=6

tab Q38,m
tab Q46 if Q38==1 // numbers do not match = 1 missing value. sbjnum = 136791565 , do we drop it?

save"$dirfolder\Data\Midline\01_preprocessing\BiH_CVE_mid_Surv2020_clean1.dta", replace
