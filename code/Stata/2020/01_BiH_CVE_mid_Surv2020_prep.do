**********************************************************************************************************		
	* 	Gustavo Castillo
	*	This do file: 
		*Relabels variables to make them easier to read
		*Collapses variables
		*Changes values to missing, where appropriate
	*	Dec 2020									    		
	*	Uses: .dta
	* 	Saves: BiHCVEMidSurv2020_boost_NACIO_FINAL_Milko_6_01_prep.dta
**************************************************************************************************************

*set working DIRECTORIES 	
global dirfolder "C:\Users\gustavo.castillo\Desktop\CVE Midline Survey Questionnaire for Bosnia and Herzegovina" 

*IMPORT data
clear
use "$dirfolder\Data\Midline\01_preprocessing\BiH_CVE_mid_Surv2020_clean1.dta", clear

*APPLY Codebook to clean variables

applyCodebook using "$dirfolder\_scripts\01_ingestion\codebooks\codebook.xlsx", rename varlab recode


*DROP unnecessary variables
drop UK0 age_category2 enum_exp Supervisor SupervGender coordinator_age coordinator_exp SupervEtn SupervGender1 SupervEtn1 s_02 s_02_Nacio Q54e RegionXX Q53 Entity Region Q39_9_opn EnumGender EnumEtn


*RENAME variable labels
label var Q12_opn "What is your current employment status? Specify"
label var Q20_10_opn "In 2020, did anyone in your house exhibit symptoms of COVID?- Other, Specify"
label var Q20_11_opn "In 2020, did anyone in your house exhibit symptoms of COVID?- Other, Specify"
label var Q24a_opn "And who committed the violence? Other, Specify"
label var Q25a_opn "And who committed the violence? Other, Specify"
label var Q33b_1_opn "Where did you encounter these messages? ISIS - Specify"
label var Q33c_1_opn "What was the message? ISIS - Specify"
label var Q33b_2_opn "Where did you encounter these messages?AlQaeda-Specify"
label var Q33c_2_opn "What was the message? AlQaeda-Specify"
label var Q33_3_opn "For which of these groups did you encounter msg? Other - Spec"
label var Q33b_3_opn "Where did you encounter these messages? Other - Specify"
label var Q33c_3_opn "What was the message? Other - Specify"
label var Q33_4_opn "For which of these groups did you encounter msg? Other - Spec"
label var Q33b_4_opn "Where did you encounter these messages? Other - Specify"
label var Q33c_4_opn "What was the message? Other - Specify"
label var Q36_opn "How would you characterize your ethnicity? Other, Specify"
label var Q38_opn "What religious group do you identify with? Other, Specify"
label var Q48_opn "What type of school did you attend? Other, Specify"
label var Q52_opn "Which political parties do you agree with the most? Other, Spec"

*COLLAPSE and label new variables

tab age_category
tabstat age_category, s(n min max)
gen age_category_grp=.
replace age_category_grp=1 if age_category>=1 & age_category<3
replace age_category_grp=2 if age_category>=3 & age_category<5
replace age_category_grp=3 if age_category>=5 & age_category<.
label var age_category_grp "Age Category (categorical)"
label def catgrp 1 "18-34" 2 "35-54" 3 "55+"
label val age_category_grp catgrp
order age_category_grp, after (age_category)
tab age_category_grp,m

gen female=gender>1
label var female "Female (Binary)"
label def femmale 1 "Female" 0 "Male"
label val female femmale
order female, after (gender)

tab ethnicity
tabstat ethnicity, s(n min max)
gen ethnicity_grp=.
replace ethnicity_grp=1 if ethnicity==1 | ethnicity==8
replace ethnicity_grp=2 if ethnicity==2 
replace ethnicity_grp=3 if ethnicity==3
replace ethnicity_grp=4 if ethnicity==4 | ethnicity==5 | ethnicity==6 | ethnicity==7
label var ethnicity_grp "Ethnicity (categorical)"
label def ethnicgrp 1 "Bosniac" 2 "Serbian" 3 "Croatian" 4 "Other"
label val ethnicity_grp ethnicgrp
order ethnicity_grp, after (ethnicity)
tab ethnicity_grp,m

gen bosniac_bin=.
replace bosniac_bin=1 if ethnicity_grp==1
replace bosniac_bin=0 if ethnicity_grp==2 | ethnicity_grp==3 | ethnicity_grp==4
label var bosniac_bin "Bosniac (binary)"
label def bosniac 1 "Bosniac" 0 "Other"
label val bosniac_bin bosniac
order bosniac_bin, after (ethnicity_grp)

gen serbian_bin=.
replace serbian_bin=1 if ethnicity_grp==2
replace serbian_bin=0 if ethnicity_grp==1 | ethnicity_grp==3 | ethnicity_grp==4
label var serbian_bin "Serbian (binary)"
label def serbianbin 1 "Serbian" 0 "Other"
label val serbian_bin serbianbin
order serbian_bin, after (bosniac_bin)

gen croatian_bin=.
replace croatian_bin=1 if ethnicity_grp==3
replace croatian_bin=0 if ethnicity_grp==1 | ethnicity_grp==2 | ethnicity_grp==4
label var croatian_bin "Croatian (binary)"
label def croatianbin 1 "Croatian" 0 "Other"
label val croatian_bin croatianbin
order croatian_bin, after (serbian_bin)

gen other_bin=.
replace other_bin=1 if ethnicity_grp==4
replace other_bin=0 if ethnicity_grp==1 | ethnicity_grp==2 | ethnicity_grp==3
label var other_bin "Other (binary)"
label def otherbin 1 "Other" 0 "Bosniac/Serbian/Croatian"
label val other_bin otherbin
order other_bin, after (croatian_bin)

gen religion_grp=.
replace religion_grp=1 if religion==1
replace religion_grp=2 if religion==2
replace religion_grp=3 if religion==3
replace religion_grp=4 if religion==4 | religion==5 | religion==6 | religion==7
label var religion_grp "Religion (categorical)"
label def relgrp 1 "Islam" 2 "Catholic" 3 "Christian Orthodox" 4 "Other"
label val religion_grp relgrp
order religion_grp, after (religion)

gen islam_bin=.
replace islam_bin=1 if religion_grp==1
replace islam_bin=0 if religion_grp==2 | religion_grp==3 | religion_grp==4
label var islam_bin "Islam (binary)"
label def islambin 1 "Islam" 0 "Other"
label val islam_bin islambin
order islam_bin, after (religion_grp)

gen catholic_bin=.
replace catholic_bin=1 if religion_grp==2
replace catholic_bin=0 if religion_grp==1 | religion_grp==3 | religion_grp==4
label var catholic_bin "Catholic (binary)"
label def cathbin 1 "Catholic" 0 "Other"
label val catholic_bin cathbin
order catholic_bin, after (islam_bin)

gen christian_bin=.
replace christian_bin=1 if religion_grp==3
replace christian_bin=0 if religion_grp==1 | religion_grp==2 | religion_grp==4
label var christian_bin "Christian (binary)"
label def christbin 1 "Christian" 0 "Other"
label val christian_bin christbin
order christian_bin, after (catholic_bin)

gen other_rel_bin=.
replace other_rel_bin=1 if religion_grp==4
replace other_rel_bin=0 if religion_grp==1 | religion_grp==2 | religion_grp==3
label var other_rel_bin "Other (binary)"
label def otherrelbin 1 "Other" 0 "Islam/Catholic/Christian Orthodox"
label val other_rel_bin otherrelbin
order other_rel_bin, after (christian_bin)

gen Q2_bin=.
replace Q2_bin=0 if Q2==1
replace Q2_bin=1 if Q2==2 | Q2==3 | Q2==4 | Q2==5
label var Q2_bin "Do you social. w people from other ethnic or religious groups? (binary)"
label def yesno 0 "No" 1 "Yes"
label val Q2_bin yesno
order Q2_bin, after (Q2)

gen Q4_1_grp=.
replace Q4_1_grp=1 if Q4_1==1 | Q4_1==2
replace Q4_1_grp=2 if Q4_1==3 | Q4_1==4
replace Q4_1_grp=3 if Q4_1==5 
label var Q4_1_grp "How many of these groups do you belong to, in total? (categorical)"
label def howmany 1 "1-2" 2 "3-4" 3 "5+"
label val Q4_1_grp howmany
order Q4_1_grp, after (Q4_1)

gen Q6_1_accept=.
replace Q6_1_accept=0 if Q6_1==1 | Q6_1==2
replace Q6_1_accept=1 if Q6_1==3 | Q6_1==4
label var Q6_1_accept "How accept would you be of ppl of diff religion? - husband or wife (binary)"
label def accepting 0 "Not accepting" 1 "Accepting"
label val Q6_1_accept accepting
order Q6_1_accept, after (Q6_1)

gen Q6_2_accept=.
replace Q6_2_accept=0 if Q6_2==1 | Q6_2==2
replace Q6_2_accept=1 if Q6_2==3 | Q6_2==4
label var Q6_2_accept "How accepting would you be of ppl. of diff. religion?-Relative by marriage(binary)"
label val Q6_2_accept accepting
order Q6_2_accept, after (Q6_2)

gen Q6_3_accept=.
replace Q6_3_accept=0 if Q6_3==1 | Q6_3==2
replace Q6_3_accept=1 if Q6_3==3 | Q6_3==4
label var Q6_3_accept "How accepting would you be of ppl. of diff. religion? - Friend (binary)"
label val Q6_3_accept accepting
order Q6_3_accept, after (Q6_3)

gen Q6_4_accept=.
replace Q6_4_accept=0 if Q6_4==1 | Q6_4==2
replace Q6_4_accept=1 if Q6_4==3 | Q6_4==4
label var Q6_4_accept "How accepting would you be of ppl. of diff. religion? - Neighbor (binary)"
label val Q6_4_accept accepting
order Q6_4_accept, after (Q6_4)

gen Q6_5_accept=.
replace Q6_5_accept=0 if Q6_5==1 | Q6_5==2
replace Q6_5_accept=1 if Q6_5==3 | Q6_5==4
label var Q6_5_accept "How accepting would you be of ppl. of diff. religion? - Colleague (binary)"
label val Q6_5_accept accepting
order Q6_5_accept, after (Q6_5)

gen Q6_6_accept=.
replace Q6_6_accept=0 if Q6_6==1 | Q6_6==2
replace Q6_6_accept=1 if Q6_6==3 | Q6_6==4
label var Q6_6_accept "How accepting would you be of ppl. of diff. religion? - Shop owner (binary)"
label val Q6_6_accept accepting
order Q6_6_accept, after (Q6_6)

gen Q6_7_accept=.
replace Q6_7_accept=0 if Q6_7==1 | Q6_7==2
replace Q6_7_accept=1 if Q6_7==3 | Q6_7==4
label var Q6_7_accept "How accepting would you be of ppl. of diff. religion? - Govt. official(binary)"
label val Q6_7_accept accepting
order Q6_7_accept, after (Q6_7)

gen Q7_1_trust=.
replace Q7_1_trust=0 if Q7_1==1 | Q7_1==2
replace Q7_1_trust=1 if Q7_1==3 | Q7_1==4
label var Q7_1_trust "How trustworthy are? - People from your religious group (binary)"
label def trustworth 0 "Not trustworthy" 1 "Trustworthy"
label val Q7_1_trust trustworth
order Q7_1_trust, after (Q7_1)

gen Q7_2_trust=.
replace Q7_2_trust=0 if Q7_2==1 | Q7_2==2
replace Q7_2_trust=1 if Q7_2==3 | Q7_2==4
label var Q7_2_trust "How trustworthy are? - People from a different religious group (binary)"
label val Q7_2_trust trustworth
order Q7_2_trust, after (Q7_2)

gen Q7_3_trust=.
replace Q7_3_trust=0 if Q7_3==1 | Q7_3==2
replace Q7_3_trust=1 if Q7_3==3 | Q7_3==4
label var Q7_3_trust "How trustworthy are? - People from your ethnic group (binary)"
label val Q7_3_trust trustworth
order Q7_3_trust, after (Q7_3)

gen Q7_4_trust=.
replace Q7_4_trust=0 if Q7_4==1 | Q7_4==2
replace Q7_4_trust=1 if Q7_4==3 | Q7_4==4
label var Q7_4_trust "How trustworthy are? - People from a different ethnic group (binary)"
label val Q7_4_trust trustworth
order Q7_4_trust, after (Q7_4)

gen Q8_will=.
replace Q8_will=0 if Q8==1 | Q8==2
replace Q8_will=1 if Q8==3 | Q8==4
label var Q8_will "How willing are people around here to help their neighbors? (binary)"
label def willing 0 "Not willing" 1 "Willing"
label val Q8_will willing
order Q8_will, after (Q8)

gen Q9_1_trust=.
replace Q9_1_trust=0 if Q9_1==1 | Q9_1==2
replace Q9_1_trust=1 if Q9_1==3 | Q9_1==4
label var Q9_1_trust "How trustworthy is? - Prime Minister of Bosnia and Herzegovina (binary)"
label val Q9_1_trust trustworth
order Q9_1_trust, after (Q9_1)

gen Q9_2_trust=.
replace Q9_2_trust=0 if Q9_2==1 | Q9_2==2
replace Q9_2_trust=1 if Q9_2==3 | Q9_2==4
label var Q9_2_trust "How trustworthy is? - Prime Minister of Republika Srpska (binary)"
label val Q9_2_trust trustworth
order Q9_2_trust, after (Q9_2)

gen Q9_3_trust=.
replace Q9_3_trust=0 if Q9_3==1 | Q9_3==2
replace Q9_3_trust=1 if Q9_3==3 | Q9_3==4
label var Q9_3_trust "How trustworthy are? - Leaders of your favorite political party (binary)"
label val Q9_3_trust trustworth
order Q9_3_trust, after (Q9_3)

gen Q9_4_trust=.
replace Q9_4_trust=0 if Q9_4==1 | Q9_4==2
replace Q9_4_trust=1 if Q9_4==3 | Q9_4==4
label var Q9_4_trust "How trustworthy are? - Police (binary)"
label val Q9_4_trust trustworth
order Q9_4_trust, after (Q9_4)

gen Q9_5_trust=.
replace Q9_5_trust=0 if Q9_5==1 | Q9_5==2
replace Q9_5_trust=1 if Q9_5==3 | Q9_5==4
label var Q9_5_trust "How trustworthy are? - Local religious officials (binary)"
label val Q9_5_trust trustworth
order Q9_5_trust, after (Q9_5)

gen Q9_6_trust=.
replace Q9_6_trust=0 if Q9_6==1 | Q9_6==2
replace Q9_6_trust=1 if Q9_6==3 | Q9_6==4
label var Q9_6_trust "How trustworthy are? - Highest religious authority (binary)"
label val Q9_6_trust trustworth
order Q9_6_trust, after (Q9_6)

gen Q10_1_agree=.
replace Q10_1_agree=0 if Q10_1==1 | Q10_1==2
replace Q10_1_agree=1 if Q10_1==3 | Q10_1==4
label var Q10_1_agree "Do you agree w statements? - My opinions are respected by comm lead(binary)"
label def agreedoes 0 "Does not agree" 1 "Agree"
label val Q10_1_agree agreedoes
order Q10_1_agree, after (Q10_1)

gen Q10_2_agree=.
replace Q10_2_agree=0 if Q10_2==1 | Q10_2==2
replace Q10_2_agree=1 if Q10_2==3 | Q10_2==4
label var Q10_2_agree "Do you agree w statements? I can make a pos. diff in my fam/commu(binary)"
label val Q10_2_agree agreedoes
order Q10_2_agree, after (Q10_2)

gen Q10_3_agree=.
replace Q10_3_agree=0 if Q10_3==1 | Q10_3==2
replace Q10_3_agree=1 if Q10_3==3 | Q10_3==4
label var Q10_3_agree "Do you agree w? Most of what I do in my day is meaningful(binary)"
label val Q10_3_agree agreedoes
order Q10_3_agree, after (Q10_3)

gen Q11_1_agree=.
replace Q11_1_agree=0 if Q11_1==1 | Q11_1==2
replace Q11_1_agree=1 if Q11_1==3 | Q11_1==4
label var Q11_1_agree "Agree/disagree? Women should be free to work outside the home (binary)"
label val Q11_1_agree agreedoes
order Q11_1_agree, after (Q11_1)

gen Q11_2_agree=.
replace Q11_2_agree=0 if Q11_2==1 | Q11_2==2
replace Q11_2_agree=1 if Q11_2==3 | Q11_2==4
label var Q11_2_agree "Agree/disagree? Women should have same chance of being elected as men(binary)"
label val Q11_2_agree agreedoes
order Q11_2_agree, after (Q11_2)

gen Q16_1_optimistic=.
replace Q16_1_optimistic=0 if Q16==1 | Q16==2
replace Q16_1_optimistic=1 if Q16==3 | Q16==4
label var Q16_1_optimistic "How optimistic are you that you can achieve a better future? (binary)"
label def optimism 0 "Pessimistic" 1 "Optimistic"
label val Q16_1_optimistic optimism
order Q16_1_optimistic, after (Q16)

gen Q17_1_satisfied=.
replace Q17_1_satisfied=0 if Q17_1==1 | Q17_1==2
replace Q17_1_satisfied=1 if Q17_1==3 | Q17_1==4
label var Q17_1_satisfied "Satisfied/dissatisfied? Education/schools (binary)"
label def satisfied 0 "Dissatisfied" 1 "Satisfied"
label val Q17_1_satisfied satisfied
order Q17_1_satisfied, after (Q17_1)

gen Q17_2_satisfied=.
replace Q17_2_satisfied=0 if Q17_2==1 | Q17_2==2
replace Q17_2_satisfied=1 if Q17_2==3 | Q17_2==4
label var Q17_2_satisfied "Satisfied/dissatisfied? Medical care/clinics and hospitals (binary)"
label val Q17_2_satisfied satisfied
order Q17_2_satisfied, after (Q17_2)

gen Q17_3_satisfied=.
replace Q17_3_satisfied=0 if Q17_3==1 | Q17_3==2
replace Q17_3_satisfied=1 if Q17_3==3 | Q17_3==4
label var Q17_3_satisfied "Satisfied/dissatisfied? Public safety/police (binary)"
label val Q17_3_satisfied satisfied
order Q17_3_satisfied, after (Q17_3)

gen Q17_4_satisfied=.
replace Q17_4_satisfied=0 if Q17_4==1 | Q17_4==2
replace Q17_4_satisfied=1 if Q17_4==3 | Q17_4==4
label var Q17_4_satisfied "Satisfied/dissatisfied? Water (binary)"
label val Q17_4_satisfied satisfied
order Q17_4_satisfied, after (Q17_4)

gen Q17_5_satisfied=.
replace Q17_5_satisfied=0 if Q17_5==1 | Q17_5==2
replace Q17_5_satisfied=1 if Q17_5==3 | Q17_5==4
label var Q17_5_satisfied "Satisfied/dissatisfied? Electricity (binary)"
label val Q17_5_satisfied satisfied
order Q17_5_satisfied, after (Q17_5)

gen Q21_1_satisfied=.
replace Q21_1_satisfied=0 if Q21==1 | Q21==2
replace Q21_1_satisfied=1 if Q21==3 | Q21==4
label var Q21_1_satisfied "How satisfied are you with your government’s response to COVID-19?(binary)"
label val Q21_1_satisfied satisfied
order Q21_1_satisfied, after (Q21)

gen Q22_1_likely=.
replace Q22_1_likely=0 if Q22_1==1 | Q22_1==2
replace Q22_1_likely=1 if Q22_1==3 | Q22_1==4
label var Q22_1_likely "How likely? COVID was transmitted via normal interactions(binary)"
label def likely 0 "Not likely" 1 "Likely"
label val Q22_1_likely satisfied
order Q22_1_likely, after (Q22_1)

gen Q22_2_likely=.
replace Q22_2_likely=0 if Q22_2==1 | Q22_2==2
replace Q22_2_likely=1 if Q22_2==3 | Q22_2==4
label var Q22_2_likely "How likely? COVID was accidentally released from a lab (binary)"
label val Q22_2_likely satisfied
order Q22_2_likely, after (Q22_2)

gen Q22_3_likely=.
replace Q22_3_likely=0 if Q22_3==1 | Q22_3==2
replace Q22_3_likely=1 if Q22_3==3 | Q22_3==4
label var Q22_3_likely "How likely? COVID-19 was deliberately released by a foreign govt.(binary)"
label val Q22_3_likely satisfied
order Q22_3_likely, after (Q22_3)

gen Q22_4_likely=.
replace Q22_4_likely=0 if Q22_4==1 | Q22_4==2
replace Q22_4_likely=1 if Q22_4==3 | Q22_4==4
label var Q22_4_likely "How likely? COVID-19 was sent by God as a consequence of our sins(binary)"
label val Q22_4_likely satisfied
order Q22_4_likely, after (Q22_4)

gen Q23_1_safe=.
replace Q23_1_safe=0 if Q23==1 | Q23==2
replace Q23_1_safe=1 if Q23==3 | Q23==4
label var Q23_1_safe "How safe do you feel in your community?(binary)"
label def safe 0 "Not safe" 1 "Safe"
label val Q23_1_safe safe
order Q23_1_safe, after (Q23)

label val Q24 .
replace Q24=0 if Q24==1
replace Q24=1 if Q24==2
replace Q24=2 if Q24==3
replace Q24=3 if Q24==4
label def freq 0 "Never" 1 "Once" 2 "Twice" 3 "Three (3) or more times"
label val Q24 freq

gen Q24_bin=.
replace Q24_bin=0 if Q24==0 
replace Q24_bin=1 if Q24==1 |Q24==2 | Q24==3
label var Q24_bin "Last 12 months, how often have you/fam. been victims of physical viol?(binary)"
label def atleasto 0 "Never" 1 "At least once"
label val Q24_bin atleasto
order Q24_bin, after (Q24)

label val Q25 .
replace Q25=0 if Q25==1
replace Q25=1 if Q25==2
replace Q25=2 if Q25==3
replace Q25=3 if Q25==4
label val Q25 freq

gen Q25_bin=.
replace Q25_bin=0 if Q25==0
replace Q25_bin=1 if Q25==1 |Q25==2 | Q25==3
label var Q25_bin "Last 12 months, how often have you/fam. witnessed phys viol?(binary)"
label val Q25_bin atleasto
order Q25_bin, after (Q25)

gen Q28_1_afraid=.
replace Q28_1_afraid=0 if Q28==1 | Q28==2
replace Q28_1_afraid=1 if Q28==3 | Q28==4
label var Q28_1_afraid "Are you afraid of a VE incident happening in your comm?(binary)"
label def afraid 0 "Not afraid" 1 "Afraid"
label val Q28_1_afraid afraid
order Q28_1_afraid, after (Q28)

gen Q31_1_positive=.
replace Q31_1_positive=0 if Q31_1==1 | Q31_1==2
replace Q31_1_positive=1 if Q31_1==3 | Q31_1==4
label var Q31_1_positive "What is your opinion of the Islamic State?(binary)"
label def positive 0 "Negative" 1 "Positive"
label val Q31_1_positive positive
order Q31_1_positive, after (Q31_1)

gen Q31_2_positive=.
replace Q31_2_positive=0 if Q31_2==1 | Q31_2==2
replace Q31_2_positive=1 if Q31_2==3 | Q31_2==4
label var Q31_2_positive "What is your opinion of Al Qaeda?(binary)"
label val Q31_2_positive positive
order Q31_2_positive, after (Q31_2)

gen Q32_1_positive=.
replace Q32_1_positive=0 if Q32_1==1 | Q32_1==2
replace Q32_1_positive=1 if Q32_1==3 | Q32_1==4
label var Q32_1_positive "What is your opinion of Russia’s support to separatists in Ukraine?(binary)"
label val Q32_1_positive positive
order Q32_1_positive, after (Q32_1)

gen Q34_1_positive=.
replace Q34_1_positive=0 if Q34==1 | Q34==2
replace Q34_1_positive=1 if Q34==3 | Q34==4
label var Q34_1_positive "How would you evaluate the work of govt secur agen in deterring VE?(binary)"
label val Q34_1_positive positive
order Q34_1_positive, after (Q34)

gen Q35_1_positive=.
replace Q35_1_positive=0 if Q35==1 | Q35==2
replace Q35_1_positive=1 if Q35==3 | Q35==4
label var Q35_1_positive "How would you eval avail of serv to prev recruit to VE or help ppl leave?(binary)"
label val Q35_1_positive positive
order Q35_1_positive, after (Q35)

gen Q43_1_agree=.
replace Q43_1_agree=0 if Q43_1==1 | Q43_1==2
replace Q43_1_agree=1 if Q43_1==3 | Q43_1==4
label var Q43_1_agree "Agree/Disagree?Officials of my rel should hold design posit. in govt(binary)"
label val Q43_1_agree agreedoes
order Q43_1_agree, after (Q43_1)

gen Q43_2_agree=.
replace Q43_2_agree=0 if Q43_2==1 | Q43_2==2
replace Q43_2_agree=1 if Q43_2==3 | Q43_2==4
label var Q43_2_agree "Agree/Disagree?Officials of my religion should help settle civil disp(binary)"
label val Q43_2_agree agreedoes
order Q43_2_agree, after (Q43_2)

gen Q43_3_agree=.
replace Q43_3_agree=0 if Q43_3==1 | Q43_3==2
replace Q43_3_agree=1 if Q43_3==3 | Q43_3==4
label var Q43_3_agree "Agree/Disagree?My religion is compatible with democracy(binary)"
label val Q43_3_agree agreedoes
order Q43_3_agree, after (Q43_3)

gen Q45_1_agree=.
replace Q45_1_agree=0 if Q45==1 | Q45==2
replace Q45_1_agree=1 if Q45==3 | Q45==4
label var Q45_1_agree "To what extent do you agree with: My religion is under threat(binary)"
label val Q45_1_agree agreedoes
order Q45_1_agree, after (Q45)
 
rename Srvyr enum_id
label var enum_id "Interviewer ID"
order enum_id, after (Q54f)

rename EnumGender1 enum_gender
label var enum_gender "Gender of interviewer"
order enum_gender, after (enum_id)

rename EnumEtn1 enum_ethnicity
label var enum_ethnicity "Ethnicity of interviewer"
order enum_ethnicity, after (enum_age)

* Redefine value labels

*Q3
label val Q3 .
replace Q3=0 if Q3==1
replace Q3=1 if Q3==2
replace Q3=2 if Q3==3
replace Q3=3 if Q3==4
replace Q3=4 if Q3==5
label def frequencyn 0 "Never" 1 "Rarely" 2 "Sometimes" 3 "Often" 4 "Always"
label val Q3 frequencyn
 
gen Q3_bin=.
replace Q3_bin=1 if Q3==3 | Q3==4
replace Q3_bin=0 if Q3==0 | Q3==1 | Q3==2
label def oftenal 0 "Never" 1 "Often/Always"
label val Q3_bin oftenal
order Q3_bin, after (Q3) 

*Q5
replace Q5=. if Q5==4

gen Q5_bin=.
replace Q5_bin=1 if Q5==2 | Q5==3
replace Q5_bin=0 if Q5==1
label def none 0 "None" 1 "Some/Almost all"
label val Q5_bin none
order Q5_bin, after (Q5)

*Q14
label val Q14 .
replace Q14=-2 if Q14==1
replace Q14=-1 if Q14==2
replace Q14=0 if Q14==3
replace Q14=1 if Q14==4
replace Q14=2 if Q14==5
label def worsen -2 "Much worse" -1 "Somewhat worse" 0 "About the same" 1 "Somewhat better" 2 "Much better"
label val Q14 worsen

gen Q14_bin=.
replace Q14_bin=1 if Q14==1 | Q14==2
replace Q14_bin=0 if Q14==-2 | Q14==-1 | Q14==0
label def worsebetter 0 "Worse" 1 "Better"
label val Q14_bin worsebetter
order Q14_bin, after (Q14) 

*Q15
label val Q15 .
replace Q15=-2 if Q15==1
replace Q15=-1 if Q15==2
replace Q15=0 if Q15==3
replace Q15=1 if Q15==4
replace Q15=2 if Q15==5
label val Q15 worsen

gen Q15_bin=.
replace Q15_bin=1 if Q15==1 | Q15==2
replace Q15_bin=0 if Q15==-2 | Q15==-1 | Q15==0
label val Q15_bin worsebetter
order Q15_bin, after (Q15) 

*Q26
label val Q26 .
replace Q26=0 if Q26==1
replace Q26=1 if Q26==2
replace Q26=2 if Q26==3
replace Q26=3 if Q26==4
replace Q26=4 if Q26==5
label val Q26 frequencyn
 
gen Q26_bin=.
replace Q26_bin=1 if Q26==3 | Q26==4
replace Q26_bin=0 if Q26==0 | Q26==1 | Q26==2
label val Q26_bin oftenal
order Q26_bin, after (Q26) 

*Q27
label val Q27_1 .
replace Q27_1=0 if Q27_1==1
replace Q27_1=1 if Q27_1==2
replace Q27_1=2 if Q27_1==3
replace Q27_1=3 if Q27_1==4
label def nothreatmod 0 "Not a threat at all" 1 "A little bit of a threat" 2 "A moderate threat" 3 "A significant threat"
label val Q27_1 nothreatmod

gen Q27_1_bin=.
replace Q27_1_bin=1 if Q27_1==2 | Q27_1==3
replace Q27_1_bin=0 if Q27_1==0 | Q27_1==1
label def threatnoyes 0 "No threat" 1 "Significant threat"
label val Q27_1_bin threatnoyes
order Q27_1_bin, after (Q27_1)

*Q29
label val Q29 .
replace Q29=-2 if Q29==1
replace Q29=-1 if Q29==2
replace Q29=0 if Q29==3
replace Q29=1 if Q29==4
replace Q29=2 if Q29==5
label val Q29 worsen

gen Q29_bin=.
replace Q29_bin=1 if Q29==1 | Q29==2
replace Q29_bin=0 if Q29==-2 | Q29==-1 | Q29==0
label val Q29_bin worsebetter
order Q29_bin, after (Q29) 

*Q30_1 - Q30_5
label val Q30_1 .
replace Q30_1=0 if Q30_1==1
replace Q30_1=1 if Q30_1==2
replace Q30_1=2 if Q30_1==3
replace Q30_1=3 if Q30_1==4
replace Q30_1=4 if Q30_1==5
label val Q30_1 frequencyn
 
gen Q30_1_bin=.
replace Q30_1_bin=1 if Q30_1==3 | Q30_1==4
replace Q30_1_bin=0 if Q30_1==0 | Q30_1==1 | Q30_1==2
label val Q30_1_bin oftenal
order Q30_1_bin, after (Q30_1)

label val Q30_2 .
replace Q30_2=0 if Q30_2==1
replace Q30_2=1 if Q30_2==2
replace Q30_2=2 if Q30_2==3
replace Q30_2=3 if Q30_2==4
replace Q30_2=4 if Q30_2==5
label val Q30_2 frequencyn
 
gen Q30_2_bin=.
replace Q30_2_bin=1 if Q30_2==3 | Q30_2==4
replace Q30_2_bin=0 if Q30_2==0 | Q30_2==1 | Q30_2==2
label val Q30_2_bin oftenal
order Q30_2_bin, after (Q30_2)

label val Q30_3 .
replace Q30_3=0 if Q30_3==1
replace Q30_3=1 if Q30_3==2
replace Q30_3=2 if Q30_3==3
replace Q30_3=3 if Q30_3==4
replace Q30_3=4 if Q30_3==5
label val Q30_3 frequencyn
 
gen Q30_3_bin=.
replace Q30_3_bin=1 if Q30_3==3 | Q30_3==4
replace Q30_3_bin=0 if Q30_3==0 | Q30_3==1 | Q30_3==2
label val Q30_3_bin oftenal
order Q30_3_bin, after (Q30_3)

label val Q30_4 .
replace Q30_4=0 if Q30_4==1
replace Q30_4=1 if Q30_4==2
replace Q30_4=2 if Q30_4==3
replace Q30_4=3 if Q30_4==4
replace Q30_4=4 if Q30_4==5
label val Q30_4 frequencyn
 
gen Q30_4_bin=.
replace Q30_4_bin=1 if Q30_4==3 | Q30_4==4
replace Q30_4_bin=0 if Q30_4==0 | Q30_4==1 | Q30_4==2
label val Q30_4_bin oftenal
order Q30_4_bin, after (Q30_4)

label val Q30_5 .
replace Q30_5=0 if Q30_5==1
replace Q30_5=1 if Q30_5==2
replace Q30_5=2 if Q30_5==3
replace Q30_5=3 if Q30_5==4
replace Q30_5=4 if Q30_5==5
label val Q30_5 frequencyn
 
gen Q30_5_bin=.
replace Q30_5_bin=1 if Q30_5==3 | Q30_5==4
replace Q30_5_bin=0 if Q30_5==0 | Q30_5==1 | Q30_5==2
label val Q30_5_bin oftenal
order Q30_5_bin, after (Q30_5)

*Q33
label val Q33 .
replace Q33=0 if Q33==1
replace Q33=1 if Q33==2
replace Q33=2 if Q33==3
replace Q33=3 if Q33==5
replace Q33=4 if Q33==6
label def neverdaily 0 "Never" 1 "Yearly or less" 2 "Monthly" 3 "Weekly" 4 "Weekly"
label val Q33 neverdaily

gen Q33_bin=.
replace Q33_bin=1 if Q33_1==1 | Q33_1==2 | Q33_1==3 | Q33_1==4
replace Q33_bin=0 if Q33_1==0 
label val Q33_bin atleasto
order Q33_bin, after (Q33)

*Q37
label val Q37 .
replace Q37=0 if Q37==1
replace Q37=1 if Q37==2
replace Q37=2 if Q37==3
replace Q37=3 if Q37==4
replace Q37=4 if Q37==5
label val Q37 frequencyn
 
gen Q37_bin=.
replace Q37_bin=1 if Q37==3 | Q37==4
replace Q37_bin=0 if Q37==0 | Q37==1 | Q37==2
label val Q37_bin oftenal
order Q37_bin, after (Q37) 

*Q40
label val Q40 .
replace Q40=0 if Q40==1
replace Q40=1 if Q40==2
replace Q40=2 if Q40==3
replace Q40=3 if Q40==4
replace Q40=4 if Q40==5
label def strongbelieve 0 "Not religious and I’m against religion" 1 "Not religious but I don’t have anything against religion" 2 "I’m not sure if I’m a believer or not" 3 "Religious but I do not accept everything my religious leaders teach" 4 "Believer and I accept everything my religious leaders teach"
label val Q40 strongbelieve

gen Q40_bin=.
replace Q40_bin=1 if Q40==4
replace Q40_bin=0 if Q40==0 | Q40==1 | Q40==2 | Q40==3
label def strongbeliever 0 "Not very religious" 1 "Strongly religious"
label val Q40_bin oftenal
order Q40_bin, after (Q40) 

*Q41
label val Q41 .
replace Q41=0 if Q41==1
replace Q41=1 if Q41==2
replace Q41=2 if Q41==3
replace Q41=3 if Q41==4
replace Q41=4 if Q41==5
label def praytime 0 "Never" 1 "A few times a year" 2 "Monthly" 3 "Weekly" 4 "Daily or more"
label val Q41 praytime

gen Q41_bin=.
replace Q41_bin=1 if Q41==3 | Q41==4
replace Q41_bin=0 if Q41==0 | Q41==1 | Q41==2 
label def strongpray 0 "Not often" 1 "Often"
label val Q41_bin strongpray
order Q41_bin, after (Q41)

*Q42 
label val Q42 .
replace Q42=0 if Q42==1
replace Q42=1 if Q42==2
replace Q42=2 if Q42==3
replace Q42=3 if Q42==4
replace Q42=4 if Q42==5
label def likelihood 0 "Not at all likely" 1 "A little bit likely" 2 "Moderately/Somewhat likely" 3 "Very likely" 4 "Definitely"
label val Q42 likelihood

gen Q42_bin=.
replace Q42_bin=1 if Q42==3 | Q42==4
replace Q42_bin=0 if Q42==0 | Q42==1 | Q42==2 
label val Q42_bin likely
order Q42_bin, after (Q42)

*Q44
label val Q44 .
replace Q44=0 if Q44==1
replace Q44=1 if Q44==2
replace Q44=2 if Q44==3
replace Q44=3 if Q44==4
replace Q44=4 if Q44==5
label val Q44 frequencyn
 
gen Q44_bin=.
replace Q44_bin=1 if Q44==3 | Q44==4
replace Q44_bin=0 if Q44==0 | Q44==1 | Q44==2
label val Q44_bin oftenal
order Q44_bin, after (Q44) 

*Q49
label val Q49 .
replace Q49=0 if Q49==1
replace Q49=1 if Q49==2
replace Q49=2 if Q49==3
replace Q49=3 if Q49==4
replace Q49=4 if Q49==5
replace Q49=5 if Q49==6
replace Q49=6 if Q49==7
replace Q49=7 if Q49==8
label def educationq 0 "Not completed any level of education and I cannot read or write" 1 "Not completed any level of education but I can read and write" 2 "Successfully completed elementary education (6th grade)" 3 "Successfully completed primary education (10th grade)" 4 "Successfully completed secondary education- high school (12th grade)" 5 "Successfully completed vocational education or higher diploma degree" 6 "Successfully completed a bachelor's degree" 7 "Successfully completed Master's degree or higher"
label val Q49 educationq

gen Q49_bin=.
replace Q49_bin=1 if Q49==5 | Q49==6 | Q49==7
replace Q49_bin=0 if Q49==0 | Q49==1 | Q49==2 | Q49==3 | Q49==4 
label def educfin 0 "High school diploma or lower degree" 1 "Vocational education or higher degree"
label val Q49_bin educfin
order Q49_bin, after (Q49) 

*Q51
label val Q51 .
replace Q51=0 if Q51==1
replace Q51=1 if Q51==2
replace Q51=2 if Q51==3
replace Q51=3 if Q51==4
label val Q51 frequencyn
 
gen Q51_bin=.
replace Q51_bin=1 if Q51==2 | Q51==3
replace Q51_bin=0 if Q51==0 | Q51==1
label def internetuse 0 "Never" 1 "Often"
label val Q51_bin internetuse
order Q51_bin, after (Q51) 

*ORDER variables
save "$dirfolder\Data\Midline\01_preprocessing\BiH_CVE_mid_Surv2020_prep.dta", replace
